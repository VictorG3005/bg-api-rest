package com.example.BG.Reto.service;

import com.example.BG.Reto.dao.UserDAO;
import com.example.BG.Reto.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    public UserDAO userDAO;

    @Override
    public List<Users> findAll() {
        List<Users> usersList = userDAO.findAll();
        return usersList;
    }

    @Override
    public Users mostrar(int usr_id) {

        Users users = userDAO.mostrar(usr_id);

        return users;
    }

    @Override
    public void guardar(Users users) {

        userDAO.guardar(users);

    }

    @Override
    public void eliminar(int usr_id) {
        userDAO.eliminar(usr_id);
    }
}
