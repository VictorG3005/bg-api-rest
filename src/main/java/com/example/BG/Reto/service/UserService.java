package com.example.BG.Reto.service;

import com.example.BG.Reto.entity.Users;

import java.util.List;

public interface UserService {

    public List<Users> findAll();

    public Users mostrar(int usr_id);

    public void guardar(Users users);

    public void eliminar(int usr_id);
}
