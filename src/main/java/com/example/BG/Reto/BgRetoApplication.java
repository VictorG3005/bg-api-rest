package com.example.BG.Reto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BgRetoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BgRetoApplication.class, args);
	}

}
