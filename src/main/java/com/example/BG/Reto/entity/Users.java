package com.example.BG.Reto.entity;

import javax.persistence.*;

@Entity
@Table(name ="bgr_users")

public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_id")
    private int usr_id;

    @Column (name = "usr_name")
    private String usr_name;

    @Column (name = "usr_password")
    private String usr_password;

    public Users(){}

    public Users(int usr_id, String usr_name, String usr_password) {
        this.usr_id = usr_id;
        this.usr_name = usr_name;
        this.usr_password = usr_password;
    }

    public int getUsr_id() {
        return usr_id;
    }

    public void setUsr_id(int usr_id) {
        this.usr_id = usr_id;
    }

    public String getUsr_name() {
        return usr_name;
    }

    public void setUsr_name(String usr_name) {
        this.usr_name = usr_name;
    }

    public String getUsr_password() {
        return usr_password;
    }

    public void setUsr_password(String usr_password) {
        this.usr_password = usr_password;
    }

    @Override
    public String toString(){
        return "Users [usr_id=" + usr_id + ", usr_name=" + usr_name + ", usr_password=" + usr_password +"]";
    }
}
