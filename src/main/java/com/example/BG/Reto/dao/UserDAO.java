package com.example.BG.Reto.dao;

import com.example.BG.Reto.entity.Users;

import java.util.List;

public interface UserDAO {
    public List<Users> findAll();

    public Users mostrar(int usr_id);

    public void guardar(Users users);

    public void eliminar(int usr_id);
}
