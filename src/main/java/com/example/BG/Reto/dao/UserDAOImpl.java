package com.example.BG.Reto.dao;

import com.example.BG.Reto.entity.Users;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO{
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Users> findAll() {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Users> query = currentSession.createQuery("from Users", Users.class);

        List<Users> users = query.getResultList();

        return users;
    }

    @Override
    public Users mostrar(int usr_id) {

        Session session = entityManager.unwrap(Session.class);

        Users users = session.get(Users.class,usr_id);

        return users;
    }
    @Transactional
    @Override
    public void guardar(Users users) {
        Session session = entityManager.unwrap(Session.class);

        session.saveOrUpdate(users);

    }
    @Transactional
    @Override
    public void eliminar(int usr_id) {
        Session session = entityManager.unwrap(Session.class);

        Query<Users> query = session.createQuery("delete from Users where usr_id= :id_user").setParameter("id_user",usr_id);
        query.executeUpdate();
    }
}
