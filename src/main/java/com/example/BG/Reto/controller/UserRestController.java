package com.example.BG.Reto.controller;

import com.example.BG.Reto.entity.Users;
import com.example.BG.Reto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    @Autowired
    private UserService userService;
    //Metodo para obtener la lista de usuarios registrados http://localhost:8080/api/v1/users
    @GetMapping("users")
    public List<Users> findAll() {
        return userService.findAll();
    }
    //Metodo para obtener un usuario en especifico a partir del ID http://localhost:8080/api/v1/users/2
    @GetMapping("/users/{usr_id}")
    public Users getUsers(@PathVariable int usr_id) {
        Users users = userService.mostrar(usr_id);

        if (users == null) {
            throw new RuntimeException("Usuario no encontrado-" + usr_id);
        }

        return users;
    }
    //Metodo para Agregar un usuario a la tabla http://localhost:8080/api/v1/users
    @PostMapping("/users")
    public Users agregarUsuario(@RequestBody Users users) {
        users.setUsr_id(0);
        userService.guardar(users);

        return users;
    }
    //Metodo para actualizar un usuario en especificando un id http://localhost:8080/api/v1/users/2
    @PutMapping("/users")
    public Users actualizarUsuario(@RequestBody Users users) {
        userService.guardar(users);

        return users;
    }
    //Metodo para eliminar un dato a base de un ID en la Base de datos http://localhost:8080/api/v1/users/2
    @DeleteMapping("users/{usr_id}")

    public String eliminarUsuario(@PathVariable int usr_id) {
        Users users = userService.mostrar(usr_id);

        if (users == null) {
            throw new RuntimeException("Usuario no encontrado- " + usr_id);
        }

        userService.eliminar(usr_id);

        return "Usuario eliminado: " + usr_id;
    }


}
