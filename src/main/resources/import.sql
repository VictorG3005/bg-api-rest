CREATE SCHEMA `bgusers` ;

create table bgr_users
(
    USR_ID       int auto_increment
        primary key not null,
    USR_NAME     varchar(25) not null,
    USR_PASSWORD varchar(75) not null
);