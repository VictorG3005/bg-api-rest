# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.example.BG Reto' is invalid and this project uses 'com.example.BG.Reto' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/#build-image)

### Documentación para instalar BD mysql y ejecutar el programa.

Paso1:
    Ejecutar el codigo Sql del archivo import.sql ubicado en la carpeta resources 
Paso 2: 
    Para la conexión del servicio con la Base de datos en caso de tener credenciales diferentes
    las cuales son (user: root, password: *Sin password*) debe dirigirse a el archivo application.properties ubicado en la carpeta resources
    
Iniciar el proyecto y probar los puntos solicitados en el documento.

##.

